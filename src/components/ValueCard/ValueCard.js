
import { Grid } from "@material-ui/core";
import { useStyles } from "./StyleValueCard";
import { ReactComponent as Arrow } from '../../img/Polygon.svg';

const ValueCard = () => {
    const cl = useStyles()
    return (
        <Grid
            container
            direction="row"
            justifyContent="space-around"
            alignItems="center"
            className={cl.root}
        >

            <Grid item >
                1
            </Grid>
            <Grid item>
                <Arrow width='15px' height='15px' className={cl.svg_arr_value_item}/>
            </Grid>


        </Grid>
    )
}


export default ValueCard;