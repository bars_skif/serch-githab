import { makeStyles } from '@material-ui/core/styles';





export const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        height: '147px',
        backgroundColor: '#DDDDDD',
        '& .MuiIconButton-root:hover':{
            backgroundColor: '#00A3FF'
        }
    },

    iconButton: {
        padding: 10,
        backgroundColor: '#00A3FF',
        borderRadius: 'inherit'
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
        // color:'red',
        fontFamily: 'Roboto'

    },

    wrap_serch_input: {
        width: '90%',
        backgroundColor: '#FFFFFF',
        border: '1px solid #A2A3A4'
    }

}));