
import { Grid, IconButton, InputBase } from "@material-ui/core";
import { ReactComponent as SerchSvg } from '../../img/Rectangle.svg';

import { useStyles } from "./StyleSerch";

const Serch = ({ setSerch, hendelSerch }) => {
    const cl = useStyles()


    return (
        <Grid
            container
            direction="row"
            justifyContent="center"
            alignItems="center"
            className={cl.root}
        >
            <Grid
                container
                direction="row"
                justifyContent="space-around"
                alignItems="center"
                className={cl.wrap_serch_input}
            >
                <InputBase
                    className={cl.input}
                    placeholder="Начните вводить текст для поиска (не менее трех символов)"
                    onChange={(e) => setSerch(e.target.value)}
                />
                <IconButton type="submit" className={cl.iconButton} aria-label="search" onClick={hendelSerch}>
                    <SerchSvg />
                </IconButton>
            </Grid>

        </Grid>
    )
}


export default Serch;