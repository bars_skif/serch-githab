import { useState } from 'react'


import { Avatar, Grid, IconButton, InputBase } from "@material-ui/core"
import { useStyles } from "./StyleCard"
import Star from '../../img/Star.svg'
import EyeSvg from '../../img/eyeSvg.svg'
import CreateIcon from '@material-ui/icons/Create';
// import CreateIcon from '@mui/icons-material/Create';

const Card = ({ st, hendelOpenGit }) => {
    console.log('state', st);
    const [commit, setCommit] = useState('')
    const cl = useStyles()

    const localCommit = () => {
        localStorage.setItem(st.id, commit)
        setCommit('')
        console.log();
    }

    return (
        <Grid
            container
            direction="row"
            justifyContent="center"
            alignItems="center"
            style={{ width: '33%', cursor: 'pointer' }}
        >
            <Grid className={cl.card_body} >
                <Grid onClick={() => hendelOpenGit(st.clone_url)}>
                    <Grid >
                        <span className={cl.title}>{st.name}</span>
                    </Grid>
                    <Grid
                        container
                        direction="row"
                        justifyContent="space-around"
                        alignItems="center"
                        className={cl.avatar_block}
                    >
                        <Avatar alt="Remy Sharp" src={st.owner.avatar_url} />0
                        <span className={cl.title}>{st.owner.login}</span>
                    </Grid>
                    <Grid
                        container
                        direction="row"
                        justifyContent="flex-start"
                        alignItems="center"
                    >
                        <img src={Star} alt='star' /> <span style={{ marginLeft: '8px', marginRight: '20px' }}> {st.score} </span>
                        <img src={EyeSvg} alt='star' style={{ marginRight: '8px' }} /> <span> {st.watchers_count} </span>
                    </Grid>
                </Grid>
                <Grid item >
                    <Grid
                        container
                        direction="row"
                        justifyContent="flex-start"
                        alignItems="center"
                        className={cl.input_wrap}
                    >
                        <InputBase
                            className={cl.input}
                            placeholder="Комментарий к проекту"
                            onChange={(e) => setCommit(e.target.value)}
                            value={commit}
                        />
                        <IconButton type="submit" className={cl.iconButton} aria-label="search" onClick={localCommit}>
                            <CreateIcon />
                        </IconButton>
                    </Grid>
                </Grid>
            </Grid>

        </Grid>
    )
}




export default Card