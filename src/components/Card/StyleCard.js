import { makeStyles } from '@material-ui/core/styles';





export const useStyles = makeStyles((theme) => ({
    root: {
        border: '1px solid black',
        marginRight: '28px',
        marginBottom: '23px'
    },
    card_body: {
        paddingTop: '7px',
        paddingLeft: '16px',
        border: '1px solid #A2A3A4',
        boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
        width: '440px',
        height: '218px',
        marginRight: '28px',
        marginBottom: '23px',

    },
    title: {
        fontSize: '20px',
        width: '80%',
        marginLeft: '16px'
    },
    avatar_block: {
        marginTop: '18px',
        marginBottom: '8px'
    },
    star: {
        marginRight: '18px'
    },
    iconButton: {
        padding: 10,
        backgroundColor: '#00A3FF',
        borderRadius: 'inherit'
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
        // color:'red',
        fontFamily: 'Roboto'

    },

    wrap_serch_input: {

        backgroundColor: '#FFFFFF',

    },
    input_wrap: {
        marginTop: '30px',
        width: '70%',
        border: '1px solid #A2A3A4',

    }

}));