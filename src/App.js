import {
  Switch,
  Route,
} from "react-router-dom";


import { rootRouter } from './routers/routers'

import './App.css';

function App() {
  return (
    <Switch>
      {rootRouter.map(({ path, Components }) => (
        <Route path={path} exact key={path}>
          <Components />
        </Route>
      ))}


    </Switch>
  );
}

export default App;
