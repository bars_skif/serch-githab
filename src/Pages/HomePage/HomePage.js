import axios from 'axios'
import { useEffect, useState } from 'react'

import { Button, Grid, TextField } from '@material-ui/core'
import Serch from '../../components/Serch'
import Card from '../../components/Card'
import { useStyles } from './StyleHomePage'
import Autocomplete from '@material-ui/lab/Autocomplete';


const options = ['10', '15', '20', '25'];


const HomePage = () => {
    const cl = useStyles()
    const [value, setValue] = useState(options[0]);
    const [state, setState] = useState([])
    const [serch, setSerch] = useState('')
    const [isSerch, setIsSerch] = useState(true)
    const [newArr, setNewArr] = useState([])
    const [page, setPage] = useState(0)


    const myObject = JSON.parse(localStorage.getItem('arr'))

    const fet = async () => {
        await axios.get(`https://api.github.com/search/repositories?q=subject&per_page=${value}`,)
            .then((response) => {
                setIsSerch(true)
                setState(response.data.items)
                console.log(response.data.items);
                setIsSerch(false)
            })
            .catch((error) => {
                // handle error

                console.log('error', error);
            })
            .then(() => {
                // always executed
            });
    }

    useEffect(() => {


        fet()
    }, [value])

    useEffect(() => {
        const ts = () => {
            var arrays = [], size = 6;
            while (state.length > 0)
                arrays.push(state.splice(0, size));
            setNewArr(arrays)
            console.log(arrays);
            localStorage.setItem('arr', JSON.stringify(arrays ))
        }
        ts()
    }, [state])

    const hendelSerch = () => {
        axios.get(`https://api.github.com/search/repositories?q=subject&q=${serch}`,)
            .then(function (response) {
                setState(response.data.items)
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .then(function () {
                // always executed
            });
    }


    const hendelOpenGit = (urlGit) => {
        window.open(urlGit)
    };


    return (
        <Grid
            container
            direction="column"
            justifyContent="center"
            alignItems="center"
            className={cl.root}
        >
            <Serch setSerch={setSerch} hendelSerch={hendelSerch} />
            <Grid
                container
                direction="row"
                justifyContent="center"
                alignItems="center"
                className={cl.wrap_body}
            >
                {isSerch ? <div>Идет поиск...</div> : (
                    myObject[page]?.map((i, key)=> (
                        <Card st={i} hendelOpenGit={hendelOpenGit} key={key}/>
                    ))
                )}

            </Grid>
            <Grid
                container
                direction="row"
                justifyContent="space-around"
                alignItems="flex-end"
                style={{ height: '28vh' }}
            >
                <Grid item xs={6} style={{ paddingLeft: '20px' }}>
                    <Autocomplete
                        value={value}
                        onChange={(event, newValue) => {
                            setValue(newValue);
                        }}
                        id="controllable"
                        options={options}
                        style={{ width: 100 }}
                        renderInput={(params) => <TextField {...params} label="Controllable" variant="outlined" />}
                    />
                </Grid>
                <Grid item xs={6}>
                    <Grid
                        container
                        direction="row"
                        alignItems="flex-end">
                        <Button onClick={() => setPage(prev => prev >=  newArr.length -1 ? 0 : prev + 1)}>
                            След
                        </Button>
                        <Button onClick={() => setPage(prev => prev <= 0 ? newArr.length -1 : prev - 1)}>
                            Пред
                        </Button>

                    </Grid>
                </Grid>
            </Grid>


        </Grid>
    )
}



export default HomePage